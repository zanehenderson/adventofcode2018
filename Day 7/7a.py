# Read the input file
inputFile = open("input_7.txt", "r")
inputLines = inputFile.readlines()
inputFile.close()

temp = dict()

# Build a list of jobs and their dependencies
# We will use a dictionary here for simplicity of algorithm
for line in inputLines:
    key = line[36:37]
    dep = line[5:6]
    if not dep in temp:
        temp[dep] = set()
    if not key in temp:
        temp[key] = {dep}
    else:
        temp[key].add(dep)

# Coerce the dictionary into an ordered list format
nodes = list()
for key, deps in temp.items():
    nodes.append([key, sorted(deps)])
nodes.sort()



# 'done' contains all nodes that have been traversed already
done = set()

result = ""
while True:
    # Update the dependencies for each node
    for node in nodes:
        for n in node[1]:
            if n in done:
                node[1].remove(n)
    # Run jobs
    for node in nodes:
        # If a job is ready to be run
        if len(node[1]) == 0:
            if not node[0] in done:
                result = result + node[0]
                done.add(node[0])
                break
    if len(done) == len(nodes):
        break

print(result)