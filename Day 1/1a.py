file = open("input_1.txt", "r")

result = 0

for line in file.readlines():
    # Strip out newline
    line = line[:-1]
    offset = int(line)

    result += offset

print(result)
