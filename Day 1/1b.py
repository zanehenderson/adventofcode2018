file = open("input_1.txt", "r")

offsets = file.readlines()
# Strip out newline chars
for i in range(len(offsets)):
    offsets[i] = int(offsets[i][:-1])

currentFreq = 0
seenFreqs = list()
seenFreqs.append(currentFreq)

found = False

# Repeat forever until a solution is found
while not found:
    for offset in offsets:
        currentFreq += offset
        if currentFreq in seenFreqs:
            # We've seen this frequency before
            found = True
            break
        seenFreqs.append(currentFreq)
    print(seenFreqs)
print(currentFreq)
