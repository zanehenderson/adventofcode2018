from datetime import datetime

inputFile = open("input_4.txt", "r")

logLines = inputFile.readlines()
inputFile.close()

# Strip out newline chars
for i in range(len(logLines)):
    logLines[i] = logLines[i][:-1]

logLines.sort()

guards = list()

for i in range(len(logLines)):
    message = logLines[i][19:]

    if message[:5] == "Guard":
        # Get the guard ID
        guardID = message[7:]
        guardID = int(guardID[:-13])
        
        # Check if we have seen this guard before
        thisGuard = None
        for g in guards:
            if g[0] == guardID:
                thisGuard = g
        
        if thisGuard == None:
            # Record doesn't exist yet, create a record
            thisGuard = list()
            thisGuard.append(guardID)
            thisGuard.append(list())
            for i in range(0, 59):
                thisGuard[1].append(0)
            guards.append(thisGuard)

    if message == "falls asleep":
        sleepMinute = int(logLines[i][15:17])
        wakeMinute = int(logLines[i + 1][15:17])
        for m in range(sleepMinute, wakeMinute):
            thisGuard[1][m] += 1

sleepiestGuard = guards[0]
for g in guards:
    if sum(g[1]) > sum(sleepiestGuard[1]):
        sleepiestGuard = g

sleepiestMinute = 0
for minute in range(59):
    if sleepiestGuard[1][minute] > sleepiestGuard[1][sleepiestMinute]:
        sleepiestMinute = minute

result = sleepiestGuard[0] * sleepiestMinute

print("Guard:", sleepiestGuard[0])
print("Minutes:", sum(sleepiestGuard[1]))
print("Sleepiest minute:", sleepiestMinute)
print("Answer:", result)

