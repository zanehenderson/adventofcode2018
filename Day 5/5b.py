# Read input from file
inputFile = open("input_5.txt", "r")
polymer = inputFile.read()
inputFile.close()

backup = polymer
shortest = len(polymer)
for i in range(65, 91):
    polymer = backup
    polymer = polymer.replace(chr(i), "")
    polymer = polymer.replace(chr(i + 32), "")

    position = 0
    # Work from beginning to end of string
    while position < len(polymer) - 1:
        if ord(polymer[position]) <= 90:
            # This letter is uppercase
            offset = 32
        else:
            # This letter is lowercase
            offset = -32

        if ord(polymer[position + 1]) == ord(polymer[position]) + offset:
            # The next letter is the opposite case
            # Cut out these two letters from the string
            #print("Found @ [{0}:{1}] - '{2}{3}'".format(position, position + 1, polymer[position], polymer[position + 1]))
            polymer = polymer[:position] + polymer[position + 2:]

            # We will have the check the previous letter again
            # Could unintentionally result in a negative position, which counts backwards at the end of the string.
            if position > 0:
                position -= 1

        else:
            # No match, continue to next pair
            position += 1

    if len(polymer) < shortest:
        shortest = len(polymer)

# Print the final result
print("Shortest length:", shortest)