def manhattanDistance(x1, y1, x2, y2):
    return abs(x2 - x1) + abs(y2 - y1)

def getNearestPointTo(x, y, points):
    nearestDist = 1000000
    nearestID = -1
    for k, p in points.items():
        md = manhattanDistance(x, y, p['x'], p['y'])
        if md == nearestDist:
            nearestID = -1
        if md < nearestDist:
            nearestDist = md
            nearestID = p['id']
    if nearestID == -1:
        # This coordinate does not have a nearest point
        # Return a dummy point instead
        return {'id': -1, 'x': -1, 'y': -1, 'infinite': True}
    return points[nearestID]

# Read the input file
inputFile = open("input_6.txt", "r")
pointLines = inputFile.readlines()
inputFile.close()

# We will store the points in an list
points = dict()
maxX = 0
maxY = 0

for i in range(len(pointLines)):
    # Strip out newline chars
    pointLines[i] = pointLines[i][:-1]

    # Get coordinates
    coords = pointLines[i].split(", ")
    x = int(coords[0])
    y = int(coords[1])

    if x > maxX:
        maxX = x
    if y > maxY:
        maxY = y

    p = dict()
    p['id'] = i
    p['x'] = x
    p['y'] = y
    p['infinite'] = False
    points[i] = p

for x in range(maxX):
    getNearestPointTo(x, 0, points)['infinite'] = True
    getNearestPointTo(x, maxY, points)['infinite'] = True
for y in range(maxY):
    getNearestPointTo(0, y, points)['infinite'] = True
    getNearestPointTo(maxX, y, points)['infinite'] = True

# For each point on the grid, we will check which point is the closest
# The grid is stored as a list of X-columns, with Y-cells.
# Each cell has an ID of the closest point, and the Manhattan distance to that point
grid = list()
for x in range(maxX):
    grid.append(list())
    for y in range(maxY):
        grid[x].append([-1, 1000000])
        p = getNearestPointTo(x, y, points)
        grid[x][y][0] = p['id']
        grid[x][y][1] = manhattanDistance(x, y, p['x'], p['y'])

# Distance grid is complete
# Do calculations for the largest enclosed area
largestAreaID = -1
largestAreaSize = 0

for k, p in points.items():
    # Skip infinite points
    if p['infinite']:
        continue

    count = 0
    for x in range(maxX):
        for y in range(maxY):
            if grid[x][y][0] == p['id']:
                count += 1

    if count > largestAreaSize:
        largestAreaSize = count
        largestAreaID = p['id']

print("ID:", largestAreaID)
print("Size:", largestAreaSize)
