def manhattanDistance(x1, y1, x2, y2):
    return abs(x2 - x1) + abs(y2 - y1)

def manhattanDistancePoint(x, y, p):
    return abs(p['x'] - x) + abs(p['y'] - y)

# Read the input file
inputFile = open("input_6.txt", "r")
pointLines = inputFile.readlines()
inputFile.close()

# We will store the points in an list
points = dict()
maxX = 0
maxY = 0

for i in range(len(pointLines)):
    # Strip out newline chars
    pointLines[i] = pointLines[i][:-1]

    # Get coordinates
    coords = pointLines[i].split(", ")
    x = int(coords[0])
    y = int(coords[1])

    if x > maxX:
        maxX = x
    if y > maxY:
        maxY = y

    p = dict()
    p['id'] = i
    p['x'] = x
    p['y'] = y
    points[i] = p

# From each coordinate, we'll sum the distance to each point
# Maintain a total of all valid coordinates
selectedArea = 0
for x in range(maxX):
    for y in range(maxY):
        sum = 0
        for k,p in points.items():
            sum += manhattanDistancePoint(x, y, p)
            if sum >= 10000:
                break
        if sum < 10000:
            selectedArea += 1

print("Size:", selectedArea)