inputFile = open("input_2.txt", "r")

boxes = inputFile.readlines()
# Strip out newline chars
for i in range(len(boxes)):
    boxes[i] = boxes[i][:-1]

for posA in range(0, len(boxes) - 1):
    for posB in range(posA + 1, len(boxes)):
        boxA = boxes[posA]
        boxB = boxes[posB]

        resultBox = ""
        for i in range(0, len(boxA)):
            if boxA[i] == boxB[i]:
                resultBox = resultBox + boxA[i]

        if len(resultBox) == len(boxA) - 1:
            print(resultBox)
            exit(0)