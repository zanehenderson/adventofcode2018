inputFile = open("input_2.txt", "r")

boxes = inputFile.readlines()
# Strip out newline chars
for i in range(len(boxes)):
    boxes[i] = boxes[i][:-1]

twoCount = 0
threeCount = 0

for box in boxes:
    # Each box only counts once
    foundTwo = False
    foundThree = False

    # Check each letter from a to z
    for c in range(ord('a'), ord('z')+1):
        if box.count(chr(c)) == 2:
            foundTwo = True
        if box.count(chr(c)) == 3:
            foundThree = True
    
    if foundTwo:
        twoCount += 1
    if foundThree:
        threeCount += 1

result = twoCount * threeCount

print(twoCount, threeCount)
print(result)