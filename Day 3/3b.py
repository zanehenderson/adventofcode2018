inputFile = open("input_3.txt", "r")

rawClaims = inputFile.readlines()
inputFile.close()

# Strip out newline chars
for i in range(len(rawClaims)):
    rawClaims[i] = rawClaims[i][:-1]

claims = list()
for rawClaim in rawClaims:
    # rawClaim is a string in the following format -> #ID @ X,Y: WxH

    # Split into sections, then process each section as needed
    claim = rawClaim.split(' ')
    # Remove useless section 1
    del claim[1]

    # Section 0 - ID
    claim[0] = int(claim[0][1:])

    # Section 1 - Offset
    claim[1] = claim[1][:-1]
    claim[1] = claim[1].split(',')
    claim[1][0] = int(claim[1][0])
    claim[1][1] = int(claim[1][1])

    # Section 2 - Dimensions
    claim[2] = claim[2].split('x')
    claim[2][0] = int(claim[2][0])
    claim[2][1] = int(claim[2][1])

    claims.append(claim)

# Create a 1000x1000 'fabric' array
fabric = list()
for i in range(0, 1000):
    x = list()
    for j in range(0, 1000):
        x.append(list())
    fabric.append(x)

for claim in claims:
    for x in range(claim[1][0], claim[1][0] + claim[2][0]):
        for y in range(claim[1][1], claim[1][1] + claim[2][1]):
            fabric[x][y].append(claim[0])

result = list(range(1,1366))
for row in fabric:
    for cell in row:
        if len(cell) > 1:
            for id in cell:
                try:
                    result.remove(id)
                except ValueError:
                    pass

print(result[0])