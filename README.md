# Advent Of Code 2018

These are my submission for [Advent of Code](https://www.adventofcode.com "Advent of Code").

I am currently working on completing this year's challenges and will update this repo occasionally.

## What is it?

Each day through December 1 to December 25, a new programming challenge is created to test programming skills.
Each participant receives the same challenge, but a different input data set.